# Bridge Creator Plugin


_Unreal Engine ver: 5.2_


A simple plugin to create bridges using Unreal Engine's "Geometry Script Plugin".


Prerequisites: "Geometry Script Plugin" has to be enabled in the project before using this plugin.


It's a blueprint only plugin - no C++ project required to use it.


<img src="Resources/HighresScreenshot00159.png" width="640" height="360" />



## Usage
copy the folder 'BridgeCreator' into the 'Plugins' folder of the project (create it if not there).

```bash
├── Plugins
│   ├── BridgeCreator
│   │   ├── Content
│   │   ├── Resources
│   │   ├── BridgeCreator.uplugin
```

to get started, simply drag the 'BP_GeoS_BridgeCreator_v2' blueprint from the plugin's Content folder into the level.


most of the settings/exposed variables should be self explanatory, but each variable has a tooltip text with a short explanation as well.


when satisfied with the result, click on the 'ConvertToStaticMesh' button to create a static mesh. note: there is no check if the asset with the same name already exists in the folder. be careful - it will overwrite it without notice!


## Known Bugs and Limitations
some things don't work (yet?)


the spline should always have 3 points. the last point will determine the bridge's length and the centre point can be used to bend the bridge up/down.


the bend can only be curved (spline point type: 'curved'). sharp edges will break the meshes. completely straight bridges/spline will work though.


if the bridge gets longer (> 1000), the 'EdgesWeldTolerance' has to be adjusted to weld the meshes together again (or, as an alternative, increase the number of 'NumSamplesOnSpline' until the mesh merges again.)





